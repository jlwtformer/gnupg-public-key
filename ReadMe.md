# Jacob Westall's Public GnuPG Key

Secondary storage for my public key. Download the file from this repo and then run `gpg --import jacobwestall.asc` to add my key to your keyring.

### Public Fingerprint

`60FC 6176 612A 1476 3045 BAA9 6894 8833 CC1F 3677`